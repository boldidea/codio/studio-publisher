const config = {};

function getBool(val) {
  return !!(val && val.match(/^(t(rue)?|(?!0)\d+)$/i));
}

config.DEBUG = getBool(process.env.DEBUG);
config.HOSTNAME = process.env.HOSTNAME || process.env.CODIO_BOX_DOMAIN || 'localhost';
config.LISTEN_PORT = process.env.LISTEN_PORT || 3000;
config.LISTEN_ADDR = process.env.LISTEN_ADDR || '0.0.0.0';
config.PROTOCOL = process.env.PROTOCOL || 'http';
config.HIVE_API_URL = process.env.HIVE_API_URL || 'https://api.boldidea.org';
config.HIVE_API_VERIFY_SSL = getBool(process.env.HIVE_API_IS_LOCAL);
config.CLIENT_ID = process.env.CLIENT_ID;
config.CLIENT_SECRET = process.env.CLIENT_SECRET;
config.PROJECT_HOME = process.env.PROJECT_HOME ||'/home/codio/workspace';
config.PROJECT_BASE_DOMAIN = process.env.PROJECT_BASE_DOMAIN || 'studio.boldidea.org';
config.PROJECT_HOST_SCHEME = process.env.PROJECT_HOST_SCHEME || 'https';
config.SCRATCH_PROJECT_NAME = process.env.SCRATCH_PROJECT_NAME || 'project.sb3';

export default config;
