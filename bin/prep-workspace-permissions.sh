#!/bin/bash

# This script ensures server.js can modify certain files, assuming the user
# running the server is a member of the `codio` group. Otherwise, the server
# would need to run as root, however that is not recommended for security
# reasons.
#
# In order for the server user to run this script using sudo, create the file
# `/etc/sudoers.d/studio-publisher` with the following line:
#
#   studio ALL= NOPASSWD: /srv/studio-publisher/bin/prep-workspace-files.sh

BASE_DIR=$(readlink -f "$(dirname $0)/..");
export $(cat "$BASE_DIR/.env" | xargs);
[[ -z "$PROJECT_HOME" ]] && PROJECT_HOME=/home/codio/workspace;

chgrp codio "$PROJECT_HOME" || exit 1;
chmod g+rwx "$PROJECT_HOME" || exit 1;
if [[ -d "$PROJECT_HOME/.git" ]]; then
    chgrp -R codio "$PROJECT_HOME/.git" || exit 1;
    chmod -R g+rw "$PROJECT_HOME/.git" || exit 1;
fi

if [[ -f "$PROJECT_HOME/.gitlab-ci.yml" ]]; then
    chgrp codio "$PROJECT_HOME/.gitlab-ci.yml" || exit 1;
    chmod g+rw "$PROJECT_HOME/.gitlab-ci.yml" || exit 1;
fi
