import fs from 'fs';
import path from 'path';
import Fastify from 'fastify';
import fastifyStatic from 'fastify-static';
import pointOfView from 'point-of-view';
import fastifyCookie from 'fastify-cookie';
import fastifyFormbody from 'fastify-formbody';
import { fileURLToPath } from 'url';
import Nunjucks from 'nunjucks';
import Profanity from '@2toad/profanity';
import config from './config.js';
import * as utils from './utils.js';
import * as api from './api.js';

const profanity = Profanity.profanity;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// // Support ${CONFIGVAR} replacement in config values
// for (const varName in config) {
//   if (config[varName].replace) {
//     config[varName] = config[varName].replace(/\\${([^}]+)\}/g, (_, key) => config[key]);
//   }
// }

// TODO
const GIT_IGNORE = [
  '.codio'
]

const fastify = Fastify({
  logger: {
    prettyPrint: true
  }
});

/**
 * plugins 
 **/
fastify.register(fastifyStatic, {
  root: path.join(__dirname, 'static'),
  prefix: '/static/'
});

fastify.register(pointOfView, {
  engine: {
    nunjucks: Nunjucks 
  }
});

fastify.register(fastifyCookie, {
  parseOptions: {}
});

fastify.register(fastifyFormbody);

const FORM_FIELDS = ['uuid', 'name', 'slug', 'description'];

/**
 * views
 **/
async function saveForm(req, context) {
  utils.prepWorkspacePermissions();

  // Profanity check
  const checkFields = {name: 'Name', slug: 'Domain', description: 'Description'};
  for (const [field, label] of Object.entries(checkFields)) {
    if (profanity.exists(req.body[field])) {
      context['error'] = `"${label}" does not pass profanity check.`;
    }
  }

  if (context['error']) {
    context['formData'] = Object.assign(req.body);
    return false;
  }

  const uuid = utils.getProjectUUID();

  // Save to API
  const data = {
    uuid,
    slug: req.body.slug,
    name: req.body.name,
    description: req.body.description
  };
  const project = await api.saveProject(req, data);

  // push to git
  await utils.gitPush(project, context['userData']);

  // Finished!
  return true;
}

async function home(req, res) {
  // The UUID is defined by the client environment
  const uuid = utils.getProjectUUID();

  let project = {uuid: uuid};
  try {
    project = await api.getProject(req, uuid);
  } catch(error) {
    // leave project object empty if not found, otherwise throw error
    if (!(error instanceof api.NotFoundError)) throw error;
  }

  const formData = {};
  for (let field of FORM_FIELDS) {
    formData[field] = project[field];
  }

  let canPublish = true;
  if (project.url && !project.is_owner) {
    canPublish = false;
  }

  const context = {
    formData, project, canPublish,
    error: req.query.error,
    userData: req.userData,
    HIVE_API_URL: config.HIVE_API_URL,
    PROJECT_BASE_DOMAIN: config.PROJECT_BASE_DOMAIN
  };

  if (req.method === 'POST') {
    const success = await saveForm(req, context);
    if (success) {
      return res.redirect('/published');
    }
  }

  return res.view('/templates/index.html', context);
}

// Allow api.auth to decorate the request
fastify.addHook('onRequest', api.auth);

fastify.route({
  method: 'GET',
  path: '/',
  handler: home
});

fastify.route({
  method: 'POST',
  path: '/',
  handler: home
});

fastify.get('/api/domain-available/:domain', async (req, res) => {
  const available = await api.domainExists(req, req.params.domain);
  return res.send({
    domain: req.params.domain,
    available: available
  });
});

fastify.get('/published', async (req, res) => {
  const uuid = utils.getProjectUUID();
  const project = await api.getProject(req, uuid);
  const context = { project };
  return res.view('/templates/published.html', context);
});

/**
 * Error handler
 */
fastify.setErrorHandler((error, req, res) => {
  console.error(error);
  return res.status(500).view('/templates/error.html', {error, debug: config.DEBUG});
});

fastify.setNotFoundHandler((req, res) => {
  return res.status(404).view('/templates/notFound.html');
});

/**
 * entrypoint
 **/
const start = async () => {
  try {
    await fastify.listen(config.LISTEN_PORT, config.LISTEN_ADDR);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
