import fs from 'fs';
import path from 'path';
import childProcess from 'child_process';
import YAML from 'yaml';
import { fileURLToPath } from 'url';
import SimpleGit from 'simple-git';
import config from './config.js';

const execSync = childProcess.execSync;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export function getProjectUUID() {
  if (!process.env.PROJECT_UUID) {
    throw new Error(
      'Could not determine project UUID. Please set the PROJECT_UUID environment variable.');
  }
  return process.env.PROJECT_UUID;
}

export function prepWorkspacePermissions() {
  /**
   * See bin/prep-workspace-files.sh for more info
   */
  const scriptPath = path.join(__dirname, 'bin', 'prep-workspace-permissions.sh');
  try {
    execSync(`sudo -n ${scriptPath}`);
  } catch (error) {
    // Allow this command to fail, but log for debugging purposes
    console.error(error);
  }
}

function getGitlabCIYaml(opts) {
  const postBuild = [];
  let buildPublicDir;
  if (opts.scratchProject) {
    buildPublicDir = [
      'git clone https://github.com/BoldIdeaInc/scratch-player public',
      'rm -rf public/.git',
      'cd public',
      'mkdir project',
      'cd project',
      `unzip ../../${opts.scratchProject}`,
      'cd ../../'
    ]
  } else {
    buildPublicDir = [
      'mkdir .public',
      'cp -r * .public',
      'mv .public public',
    ]
  }
  opts.removePaths = opts.removePaths || [];
  const yaml = {
    pages: {
      stage: 'deploy',
      script: [
        ...buildPublicDir,
        ...opts.removePaths.map(f => `rm -rf public/${f}`)
      ],
      artifacts: {
        paths: ['public']
      },
      only: ['master']
    }
  };
  return yaml;
}


export async function gitPush(project, userInfo) {
  const GITLAB_CI_PATH = `${config.PROJECT_HOME}/.gitlab-ci.yml`;
  const GIT_IGNORE = [ // TODO
    '.codio'
  ];
  const PUBLISH_REMOVE_PATHS = [
    ...GIT_IGNORE,
    '.git',
    GITLAB_CI_PATH,
  ];

  if (!project.ssh_key) throw new Error(`ssh key missing for project id=${project.id}`);
  if (!project.ssh_pubkey) throw new Error(`ssh pubkey missing for project id=${project.id}`);

  // Write the SSH key to temporary location
  const tmpDir = fs.mkdtempSync('/tmp/ssh-');
  try {
    const sshKeyPath = path.join(tmpDir, 'id_rsa');
    const sshPubKeyPath = path.join(tmpDir, 'id_rsa.pub');
    fs.chmodSync(tmpDir, 0o700);
    fs.writeFileSync(sshKeyPath, project.ssh_key, {mode: 0o600});
    fs.writeFileSync(sshPubKeyPath, project.ssh_pubkey, {mode: 0o600});

    // Write .gitlab-ci.yml
    const gitlabCIOpts = {
      removePaths: PUBLISH_REMOVE_PATHS
    };
    const scratchProjectPath = path.join(config.PROJECT_HOME, config.SCRATCH_PROJECT_NAME);
    if (fs.existsSync(scratchProjectPath)) {
      gitlabCIOpts.scratchProject = config.SCRATCH_PROJECT_NAME;
    }
    const gitlabCIYaml = getGitlabCIYaml(gitlabCIOpts);
    fs.writeFileSync(GITLAB_CI_PATH, YAML.stringify(gitlabCIYaml));

    // Setup git options
    const sshOpts = '-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o IdentitiesOnly=yes';
    const sshCommand = `ssh ${sshOpts} -i ${sshKeyPath}`;

    // Get git instance for PROJECT_HOME
    const git = SimpleGit({baseDir: config.PROJECT_HOME}).env('GIT_SSH_COMMAND', sshCommand);

    // initialize repo
    await git.init();
    await git.addConfig('user.name', userInfo.name);
    await git.addConfig('user.email', userInfo.email);

    // create remote "origin" if not exists
    const remotes = await git.getRemotes();
    const origin = remotes.find(remote => remote.name === 'origin');
    if (!origin || origin.push !== project.repo_url) {
      if (origin) await git.removeRemote('origin');
      git.addRemote('origin', project.repo_url);
    }

    // Add, commit, and push
    const commitMessage = 'Published to Bold Idea Studio';
    // We use --force here because the publisher tool is not meant to resolve conflicts.
    // The default behavior is to override whatever is currently published in master.
    await git.add('.').commit(commitMessage).push('origin', 'master', {'--force': null});
  } finally {
    // clean up
    fs.rmdirSync(tmpDir, {recursive: true});
  }
}
