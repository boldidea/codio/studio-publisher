import baseFetch from 'node-fetch';
import https from 'https';
import config from './config.js';
import * as utils from './utils.js';

const httpsAgent = new https.Agent({
  rejectUnauthorized: config.HIVE_API_VERIFY_SSL
});

async function fetch(url, opts) {
  if (url.match(/^https:\/\//)) {
    opts['agent'] = httpsAgent;
  }
  return await baseFetch(url, opts);
}

export class ValidationError extends Error {
  constructor(data) {
    super(JSON.stringify(data));
    this.name = 'ValidationError';
    this.errors = data;
  }
}

export class NotFoundError extends Error {
  constructor(data) {
    super(JSON.stringify(data));
    this.name = 'NotFound';
  }
}

export async function auth(req, res) {
  let domain = req.hostname;
  if (domain.match(/^localhost:/)) {
    domain = 'localhost';
  }

  // Get auth token
  if (!req.cookies.authToken) {
    if (req.query.token === 'INVALID_REQUEST_TOKEN') {
      throw new Error('Error while getting token: server responded INVALID_REQUEST_TOKEN');
    }
    if (req.query.token) {
      return res.setCookie('authToken', req.query.token, {
        domain: domain,
        path: '/'
      }).redirect('/');
    } else {
      // get a request token from Hive
      const tokenRes = await fetch(`${config.HIVE_API_URL}/sso/request-token`, {
        method: 'get',
        headers: {
          'Authorization': `ClientApp ${config.CLIENT_ID}:${config.CLIENT_SECRET}`
        }
      });
      if (!tokenRes.ok) {
        let responseError;
        const responseText = await tokenRes.text();
        if (tokenRes.status === 500) {
          responseError = 'The server returned a 500 response';
        } else {
          try {
            const responseData = JSON.parse(responseText);
            responseError = responseData.detail;
            console.log('responseData:', responseData);
          } catch(e) {
            responseError = responseText;
          }
        }
        throw new Error(`Error fetching request token [${tokenRes.status}]: ${responseError}`);
      }
      const resData = await tokenRes.json();
      const requestToken = resData.request_token;
      const authURL = resData.auth_url;
      const next = encodeURIComponent(`${config.PROTOCOL}://${req.hostname}/`);

      return res.redirect(`${authURL}?next=${next}&request_token=${requestToken}`);
    }
  }
  // Check if current token is valid
  const authToken = req.cookies.authToken;
  const userDataRes = await fetch(`${config.HIVE_API_URL}/users/user-info`, {
    headers: {'Authorization': `AuthToken ${authToken}`}
  });
  if (!userDataRes.ok) {
    console.log('Current token is invalid, clearing and redirecting to /');
    return res.clearCookie('authToken', {
      domain: domain,
      path: '/'
    }).redirect('/');
  }
  const userData = await userDataRes.json();

  req.authToken = authToken;
  req.userData = userData;
}

export async function getProject(req, uuid) {
  const opts = {
    headers: {
      'Authorization': `AuthToken ${req.authToken}`,
      'Content-Type': 'application/json'
    },
    method: 'GET'
  };
  const url = `${config.HIVE_API_URL}/studio/projects/${uuid}`;
  const apiPostRes = await fetch(url, opts);
  if (apiPostRes.ok) {
    return await apiPostRes.json();
  }
  if (apiPostRes.status === 404) {
    throw new NotFoundError(`Could not find project with uuid: ${uuid}`);
  }
  throw new Error(`Could not fetch project: ${apiPostRes.statusText}`);
}

export async function saveProject(req, data) {
  const opts = {
    headers: {
      'Authorization': `AuthToken ${req.authToken}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  };

  // check if project exists
  let existingProject = null;
  try {
    existingProject = await getProject(req, data.uuid);
  } catch (error) {
    if (!(error instanceof NotFoundError)) throw error;
  }

  let url;
  if (existingProject) {
    url = `${config.HIVE_API_URL}/studio/projects/${data.uuid}`;
    opts.method = 'patch';
  } else {
    url = `${config.HIVE_API_URL}/studio/projects`;
    opts.method = 'post';
  }

  const apiPostRes = await fetch(url, opts);
  if (!apiPostRes.ok) {
    if (apiPostRes.status === 400) {
      throw new ValidationError(await apiPostRes.json());
    }
    throw new Error(`Could not save project: ${apiPostRes.statusText}`);
  } else if (apiPostRes.status === 404) {
    // project was deleted server-side?
    throw new Error(`Could not save project: project not found with id ${data.uuid}`);
  }
  const project = await apiPostRes.json();
  return project;
}

export async function domainExists(req, domain) {
  const domainRes = await fetch(`${config.HIVE_API_URL}/studio/projects?slug=${req.params.domain}`, {
    headers: {'Authorization': `AuthToken ${req.authToken}`}
  });
  if (!domainRes.ok) throw new Error(`${domainRes.status}: ${domainRes.statusText}`);
  const domains = await domainRes.json();
  return domains.length === 0;
}
