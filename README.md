# Bold Idea Studio Publisher

This web app is intended to run on a Codio box to allow students to publish their projects to GitLab pages.

## Installing on a bare Codio stack

Because this webapp requires secrets in order to communicate w/ the Hive api, admin privileges must be revoked from the 
`codio` user.

The web app can then be installed along with a few server configs:

1. Create an admin user, eg, `boldidea`, and add to the following groups: `sudo`, `adm`, `systemd-journal`.
2. Remove `codio` user from `sudo`, `adm`, and `systemd-journal` groups.
3. Create the user `studio` with group `studio`.
4. Clone this repo into `/srv/studio-publisher`
5. Set ownership to `studio:studio`: `chown -R studio:studio /srv/studio-publisher`
6. Copy the following config files from the `server-configs` dir:
   - `/lib/systemd/system/studio-publisher.service`
   - `/etc/sudoers.d/studio-publisher`
   - `/etc/profile.d/codio-project-uuid.sh`
7. Enable the service: `sudo systemctl enable studio-publisher.service`
8. Create the `/srv/studio-publisher/.env` file with the following contents:
   ```
   CLIENT_ID=XXXXXXXXXXXXXXXX
   CLIENT_SECRET=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   LISTEN_ADDR=0.0.0.0
   LISTEN_PORT=3000
   PROTOCOL=https
   HIVE_API_URL=https://my.boldidea.org
   PROJECT_HOME=/home/codio/workspace
   ```
   Set `CLIENT_ID` and `CLIENT_SECRET` according to the client app set up in the Hive admin.
9. Start the service: `sudo systemctl start studio-publisher`

## Starter pack
Once you have the base stack ready to go, you'll want to create a
starter pack with a menu item for the publisher. The starter pack should
have a "Project Publisher" entry in the `.codio` json file that points
to `https://{{domain3000}`:

```json
  ...
  "preview": {
    "Project Index (static)": "https://{{domain}}/",
    "Current File (static)": "https://{{domain}}/{{filepath}}",
    "Project Publisher": "https://{{domain3000}}"
  }
  ...
```

## Troubleshooting in production (on a codio box)
Logs can be viewed in a codio box by opening a terminal, switching to an
admin user (eg: `su - boldidea`), and running:

```
sudo journalctl -u studio-publisher
```

To see the last 100 lines in the log:
```
sudo journalctl -u studio-publisher -n 100
```

To see updates to the log in realtime:
```
sudo journalctl -fu studio-publisher
```
