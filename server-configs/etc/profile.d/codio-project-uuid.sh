# This uses the workspace mountpoint to determine the project UUID
export PROJECT_UUID=$(mount | grep 'on /home/codio/workspace' | perl -pe 's/^.*?storage\/([a-fA-F0-9-]+).img.*$/\1/g');
